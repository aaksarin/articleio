// check if DOM is fully loaded
/*
$(function() {
    if (window.location.pathname == '/article/') {
        newArticle();
    } else {
        showArticle(window.location.pathname);
    }
});
*/

function showArticle(article) {
    var quillOptions = {
        formats: ['bold', 'italic', 'link', 'blockquote', 'header'],
        theme: 'bubble',
    }
    quill = new Quill('#editor-container', quillOptions);

    var Delta = Quill.import('delta');
    var delta = new Delta();

    
    $("#title").html(article.title);
    $("#author").html(article.author);

    quill.setContents(article.delta);

    //quill.disable();
    $("#edit-button").hide();
    $("#publish-button").hide();
}


function newArticle() {
    var quillOptions = {
        formats: ['bold', 'italic', 'link', 'blockquote', 'header'],
        theme: 'bubble',
    }
    quill = new Quill('#editor-container', quillOptions);

    $("#publish-button").click(publish);
    $("#publish-button").show();
    $("#edit-button").hide();
}


function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


function publish() {
    var jsonDelta = JSON.stringify(quill.getContents());
    console.log(jsonDelta);
    var title = document.getElementById("title").innerText;
    var author = document.getElementById("author").innerText;
    var data = {
        title: title, 
        author: author, 
        article: jsonDelta,
    };
    var csrftoken = getCookie('csrftoken');

    $.ajaxSetup({
            headers:
            { 'X-CSRFTOKEN': csrftoken }
        });

    request = $.ajax({
                url: "publish/",
                method: "POST",
                data: data,
                datatype: "json"
            });
    
    request.done(function(msg) {
                alert("succesfully published");
                $("#edit-button").hide();
            });

    request.fail(function(jqXHR, textStatus) {
        alert("fail");
    });
}