import json
import uuid

from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from .models import Article


def new_article(request):
    return render(request, 'article/new_article.html')

def publish(request):
    article = Article()
    article.title = request.POST["title"]
    article.author = request.POST["author"]
    print(request.POST["article"])
    article.quill_delta = json.loads(request.POST["article"])
    article.name = str(uuid.uuid4())
    article.save()

    return HttpResponse(article.name)

def show_article(request, name):
    article = get_object_or_404(Article, name=name)
    print(name)


    return render(request, 'article/show_article.html', {'article': article})
