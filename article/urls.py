from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.new_article, name='new_article'),
    url(r'^publish/$', views.publish, name='publish'),
    url(r'^(?P<name>[-a-z0-9]+)/$', views.show_article, name='show_article'),
]