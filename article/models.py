from django.contrib.postgres.fields import JSONField
from django.db import models


class Article(models.Model):
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200, default="")
    name = models.CharField(max_length=250, unique=True)
    quill_delta = JSONField()

    def __str__(self):
        return self.title
